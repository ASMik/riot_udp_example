#include "periph/gpio.h"
#include <stdio.h>

#include "shell.h"
#include "msg.h"

#define MAIN_QUEUE_SIZE     (8U)
static msg_t _main_msg_queue[MAIN_QUEUE_SIZE];
extern int udp_cmd(int argc, char **argv);
static const shell_command_t shell_commands[] = {
    { "udp", "send data over UDP and listen on UDP ports", udp_cmd },
    { NULL, NULL, NULL }
};


int main(void){
    /* we need a message queue for the thread running the shell in order to
     * receive potentially fast incoming networking packets */
    gpio_init(GPIO_PIN(PORT_C, 13), GPIO_OUT);
    gpio_clear(GPIO_PIN(PORT_C, 13));
    msg_init_queue(_main_msg_queue, MAIN_QUEUE_SIZE);
    puts("Test application for the enc28j60 driver\n");
    puts("This test just pulls in parts of the GNRC network stack, use the\n"
         "provided shell commands (i.e. ifconfig, ping6) to interact with\n"
         "your enc28j60 device.\n");
    gpio_clear(GPIO_PIN(PORT_C, 13));
    /* start shell */
    puts("Starting the shell now...");
    char line_buf[SHELL_DEFAULT_BUFSIZE];
    shell_run(shell_commands, line_buf, SHELL_DEFAULT_BUFSIZE);

}
